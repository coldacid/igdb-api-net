﻿namespace IgdbApi.Search
{
    /// <summary>
    /// Represents the operation to perform for a search filter.
    /// </summary>
    public enum FilterOperation
    {
        /// <summary>Return only results where the field value is greater than the filter value.</summary>
        /// <remarks>This operation only applies for numeric fields.</remarks>
        [SearchFilterQueryTerm("gt")]
        GreaterThan,

        /// <summary>Return only results where the field value is greater than or equal to the filter value.</summary>
        /// <remarks>This operation only applies for numeric fields.</remarks>
        [SearchFilterQueryTerm("gteq")]
        GreaterThanOrEqual,

        /// <summary>Return only results where the field value is less than the filter value.</summary>
        /// <remarks>This operation only applies for numeric fields.</remarks>
        [SearchFilterQueryTerm("lt")]
        LessThan,

        /// <summary>Return only results where the field value is less than or equal to the filter value.</summary>
        /// <remarks>This operation only applies for numeric fields.</remarks>
        [SearchFilterQueryTerm("lt")]
        LessThanOrEqual,

        /// <summary>Return only results where the field value contains the filter value.</summary>
        /// <remarks>This operation performs a basic full text search on the field being filtered.</remarks>
        [SearchFilterQueryTerm("cont")]
        Contains,

        /// <summary>Return only results where the field value is an exact match to the filter value.</summary>
        [SearchFilterQueryTerm("eq")]
        Equal,

        /// <summary>Return only results where the field value is not an exact match to the filter value.</summary>
        [SearchFilterQueryTerm("noteq")]
        NotEqual,

        /// <summary>Return only results where the field value is not set.</summary>
        /// <remarks>When this operation is selected, the filter value is not required.</remarks>
        [SearchFilterQueryTerm("null")]
        Null,

        /// <summary>Return only results where the field value is set, regardless of the field value.</summary>
        /// <remarks>When this operation is selected, the filter value is not required.</remarks>
        [SearchFilterQueryTerm("present")]
        Present,

        /// <summary>Return only results where the field value contains one or more elements in the filter value.</summary>
        /// <remarks>
        /// This operation treats the filter value as a comma separated list of values, and returns records where the
        /// selected field contains at least one of the values in the list.
        /// </remarks>
        [SearchFilterQueryTerm("in")]
        InSet,

        /// <summary>Return only results where the field value contains all elements in the filter value.</summary>
        /// <remarks>
        /// This operation treats the filter value as a comma separated list of values, and returns records where the
        /// selected field contains all of the values in the list.
        /// </remarks>
        [SearchFilterQueryTerm("inall")]
        InAll,

        /// <summary>Return only results where the field value starts with the filter value.</summary>
        /// <remarks>This operation only applies for text fields.</remarks>
        [SearchFilterQueryTerm("prefix")]
        Prefix
    }
}