﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IgdbApi
{
    /// <summary>
    /// Specifies the term used for building a query parameter for a search filter with particular properties.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field)]
    public class SearchFilterQueryTermAttribute : Attribute
    {
        /// <summary>
        /// Gets the term string used in creating the query parameter name.
        /// </summary>
        public string Term { get; private set; }

        /// <summary>
        /// Instantiates a new instance of <see cref="SearchFilterQueryTermAttribute"/> with the provided term string.
        /// </summary>
        /// <param name="term">The term string to use for the decorated filter property.</param>
        /// <exception cref="ArgumentNullException"><paramref name="term"/> is <c>null</c> or an empty string.</exception>
        /// <exception cref="ArgumentException"><paramref name="term"/> contains only whitespace characters.</exception>
        public SearchFilterQueryTermAttribute(string term)
        {
            Contract.Requires<ArgumentNullException>(!String.IsNullOrWhiteSpace(term), nameof(term));

            Term = term.Trim();
        }
    }
}
