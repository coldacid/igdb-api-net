﻿using System;
using System.Diagnostics.Contracts;
using System.Reflection;

namespace IgdbApi.Search
{
    /// <summary>
    /// Represents a filter parameter for a search operation.
    /// </summary>
    /// <typeparam name="TFieldEnum"></typeparam>
    public abstract class SearchFilterBase<TFieldEnum>
    {
        internal SearchFilterBase()
        {
            Contract.Requires<InvalidOperationException>(typeof(TFieldEnum).GetTypeInfo().IsEnum,
                "TFieldEnum type parameter must be an enumeration");
        }

        /// <summary>
        /// Gets or sets the field on which the filter will operate.
        /// </summary>
        public TFieldEnum Field { get; set; }

        /// <summary>
        /// Gets or sets the operation that will be used to perform filtering.
        /// </summary>
        public FilterOperation Operation { get; set; }

        /// <summary>
        /// Gets or sets the value that will be filtered against.
        /// </summary>
        public string Value { get; set; }
    }
}
