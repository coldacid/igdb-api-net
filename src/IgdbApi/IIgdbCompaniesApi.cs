﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Threading.Tasks;
using IgdbApi.Models;

namespace IgdbApi
{
    /// <summary>
    /// Defines the interface for querying company information from the Internet Games Database.
    /// </summary>
    [ContractClass(typeof(IIgdbCompaniesApiContract))]
    public interface IIgdbCompaniesApi
    {
        /// <summary>
        /// Retrieves a list of companies as an asynchronous operation.
        /// </summary>
        /// <param name="limit">The limit of how many results should be returned.</param>
        /// <param name="offset">The offset into the list of games where results should be returned from.</param>
        /// <returns>A list of company summaries.</returns>
        /// <remarks>
        /// <para>If unset, <paramref name="limit"/> defaults to 25 results, which is the maximum allowed per call.</para>
        /// <para>If unset, <paramref name="offset"/> defaults to 0.</para>
        /// </remarks>
        Task<IEnumerable<CompanySummary>> GetCompaniesAsync(int? limit = null, int? offset = null);

        /// <summary>
        /// Retrieve's a specific company's information as an asynchronous operation.
        /// </summary>
        /// <param name="companyId">The ID of the company to look up.</param>
        /// <returns>The full information of the specified company.</returns>
        Task<Company> GetCompanyAsync(int companyId);

        /// <summary>
        /// Retrieves a list of games related to a specified company as an asynchronous operation.
        /// </summary>
        /// <param name="companyId">The ID of the company whose games are being looked up.</param>
        /// <param name="limit">The limit of how many results should be returned.</param>
        /// <param name="offset">The offset into the list of games where results should be returned from.</param>
        /// <returns>A list of company summaries.</returns>
        /// <remarks>
        /// <para>If unset, <paramref name="limit"/> defaults to 25 results, which is the maximum allowed per call.</para>
        /// <para>If unset, <paramref name="offset"/> defaults to 0.</para>
        /// </remarks>
        Task<IEnumerable<GameSummary>> GetCompanyGamesAsync(int companyId, int? limit = null, int? offset = null);

        /// <summary>
        /// Retrieves information about the list of companies known to IGDB.
        /// </summary>
        /// <returns>An object describing the meta information for the companies collection.</returns>
        Task<CompaniesMetaInfo> GetCompaniesMetaInfoAsync();
    }
}