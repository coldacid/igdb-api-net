﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Threading.Tasks;
using IgdbApi.Models;
using IgdbApi.Search;

namespace IgdbApi
{
    /// <summary>
    /// Defines the interface for querying game information from the Internet Games Database.
    /// </summary>
    [ContractClass(typeof(IIgdbGamesApiContract))]
    public interface IIgdbGamesApi
    {
        /// <summary>
        /// Retrieves a list of games as an asynchronous operation.
        /// </summary>
        /// <param name="limit">The limit of how many results should be returned.</param>
        /// <param name="offset">The offset into the list of games where results should be returned from.</param>
        /// <returns>A list of game summaries.</returns>
        /// <remarks>
        /// <para>If unset, <paramref name="limit"/> defaults to 25 results, which is the maximum allowed per call.</para>
        /// <para>If unset, <paramref name="offset"/> defaults to 0.</para>
        /// </remarks>
        Task<IEnumerable<GameSummary>> GetGamesAsync(int? limit = null, int? offset = null);

        /// <summary>
        /// Retrieves a list of games matching the search query and optional search filters as an asynchronous operation.
        /// </summary>
        /// <param name="query">A query term to search the games with.</param>
        /// <param name="filters">A set of filters used to limit the result set.</param>
        /// <returns>A list of game summaries matching the search query and filters.</returns>
        Task<IEnumerable<GameSummary>> SearchGamesAsync(string query, params GameSearchFilter[] filters);

        /// <summary>
        /// Retrieves a specific game's information as an asynchronous operation.
        /// </summary>
        /// <param name="gameId">The ID of the game to look up.</param>
        /// <returns>The full information of the specified game.</returns>
        Task<Game> GetGameAsync(int gameId);

        /// <summary>
        /// Retrieves information about the list of games known to IGDB.
        /// </summary>
        /// <returns>An object describing the meta information for the games collection.</returns>
        Task<GamesMetaInfo> GetGamesMetaInfoAsync();
    }
}