﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Flurl;
using Flurl.Http;
using IgdbApi.Models;
using IgdbApi.Models.Wrappers;

namespace IgdbApi
{
    public partial class IgdbApi : IIgdbCompaniesApi
    {
        private static readonly string CompaniesUrl = BaseUrl.AppendPathSegment("companies");

        /// <summary>
        /// Retrieves a list of companies as an asynchronous operation.
        /// </summary>
        /// <param name="limit">The limit of how many results should be returned.</param>
        /// <param name="offset">The offset into the list of games where results should be returned from.</param>
        /// <returns>A list of company summaries.</returns>
        /// <remarks>
        /// <para>If unset, <paramref name="limit"/> defaults to 25 results, which is the maximum allowed per call.</para>
        /// <para>If unset, <paramref name="offset"/> defaults to 0.</para>
        /// </remarks>
        public async Task<IEnumerable<CompanySummary>> GetCompaniesAsync(int? limit = null, int? offset = null)
        {
            Url reqUrl = CompaniesUrl;
            if (limit.HasValue)
                reqUrl.SetQueryParam("limit", limit.Value);
            if (offset.HasValue)
                reqUrl.SetQueryParam("offset", offset.Value);

            using (var client = Client)
            {
                var list = await client.WithUrl(reqUrl).GetJsonAsync<GetListWrapper>();
                return list?.Companies ?? Enumerable.Empty<CompanySummary>();
            }
        }

        /// <summary>
        /// Retrieve's a specific company's information as an asynchronous operation.
        /// </summary>
        /// <param name="companyId">The ID of the company to look up.</param>
        /// <returns>The full information of the specified company.</returns>
        public async Task<Company> GetCompanyAsync(int companyId)
        {
            var reqUrl = CompaniesUrl.AppendPathSegment(companyId.ToString());

            using (var client = Client)
            {
                var wrapper = await client.WithUrl(reqUrl).GetJsonAsync<GetWrapper>();
                return wrapper?.Company;
            }
        }

        /// <summary>
        /// Retrieves a list of games related to a specified company as an asynchronous operation.
        /// </summary>
        /// <param name="companyId">The ID of the company whose games are being looked up.</param>
        /// <param name="limit">The limit of how many results should be returned.</param>
        /// <param name="offset">The offset into the list of games where results should be returned from.</param>
        /// <returns>A list of company summaries.</returns>
        /// <remarks>
        /// <para>If unset, <paramref name="limit"/> defaults to 25 results, which is the maximum allowed per call.</para>
        /// <para>If unset, <paramref name="offset"/> defaults to 0.</para>
        /// </remarks>
        public async Task<IEnumerable<GameSummary>> GetCompanyGamesAsync(int companyId, int? limit = null, int? offset = null)
        {
            var reqUrl = CompaniesUrl
                .AppendPathSegment(companyId.ToString())
                .AppendPathSegment("games");
            if (limit.HasValue)
                reqUrl.SetQueryParam("limit", limit.Value);
            if (offset.HasValue)
                reqUrl.SetQueryParam("offset", offset.Value);

            using (var client = Client)
            {
                var list = await client.WithUrl(reqUrl).GetJsonAsync<GetListWrapper>();
                return list?.Games ?? Enumerable.Empty<GameSummary>();
            }
        }

        /// <summary>
        /// Retrieves information about the list of companies known to IGDB.
        /// </summary>
        /// <returns>An object describing the meta information for the companies collection.</returns>
        public Task<CompaniesMetaInfo> GetCompaniesMetaInfoAsync()
        {
            using (var client = Client)
            {
                var reqUrl = CompaniesUrl.AppendPathSegment("meta");
                return client.WithUrl(reqUrl).GetJsonAsync<CompaniesMetaInfo>();
            }
        }
    }
}