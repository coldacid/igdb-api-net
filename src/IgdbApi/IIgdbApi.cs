﻿namespace IgdbApi
{
    /// <summary>
    /// Defines the interface for the IGDB public API.
    /// </summary>
    public interface IIgdbApi : IIgdbGamesApi, IIgdbCompaniesApi, IIgdbPeopleApi, IIgdbFranchisesApi, IIgdbPlatformsApi
    {
        /// <summary>
        /// Gets the API key used for requests to the IGDB API.
        /// </summary>
        string ApiKey { get; }
    }
}