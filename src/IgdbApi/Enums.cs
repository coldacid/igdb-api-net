﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Reflection;

namespace IgdbApi
{
    /// <summary>
    /// Provides extension methods for enumeration types.
    /// </summary>
    internal static class Enums
    {
        /// <summary>
        /// Retrieves the defined values in the specified enumeration type.
        /// </summary>
        /// <typeparam name="TEnum">The enumeration type to enumerate.</typeparam>
        /// <returns>A sequence of values that are defined in the specified enumeration type.</returns>
        [SuppressMessage("Microsoft.Contracts", "CC1036", Justification = "IntrospectionExtensions.GetTypeInfo() can be assumed to be [Pure]")]
        public static IEnumerable<TEnum> GetValues<TEnum>() where TEnum : struct
        {
            Contract.Requires<InvalidOperationException>(typeof (TEnum).GetTypeInfo().IsEnum,
                "Type parameter must be an enumeration type");
            Contract.Ensures(Contract.Result<IEnumerable<TEnum>>() != null);

            return Enum.GetValues(typeof (TEnum)).Cast<TEnum>();
        }

        /// <summary>
        /// Retrieves the name of the constant in an enumeration that has the specified value.
        /// </summary>
        /// <typeparam name="TEnum">The enumeration type to which <paramref name="value"/> belongs.</typeparam>
        /// <param name="value">The value of to look up from the <typeparamref name="TEnum"/> enumeration.</param>
        /// <returns>
        /// A string containing the name of the enumerated constant whose value is <paramref name="value"/>; or
        /// <c>null</c> if no such constant is found.
        /// </returns>
        public static string GetName<TEnum>(this TEnum value) where TEnum : struct
        {
            Contract.Requires<InvalidOperationException>(typeof (TEnum).GetTypeInfo().IsEnum,
                "Type parameter must be an enumeration type");

            var type = value.GetType();
            return Enum.GetName(type, value);
        }

        /// <summary>
        /// Retrieves the attributes of a specified type associated with the provided enumeration constant.
        /// </summary>
        /// <typeparam name="TAttribute">The type of attributes to return.</typeparam>
        /// <param name="value">The enumeration constant to look up.</param>
        /// <returns>
        /// A sequence of custom attributes of the specified type decorating the provided enumeration constant.
        /// </returns>
        /// <exception cref="ArgumentException">
        /// <paramref name="value"/> is not defined in its enumeration type.
        /// </exception>
        public static IEnumerable<TAttribute> GetAttributes<TAttribute>(this Enum value) where TAttribute : Attribute
        {
            Contract.Requires<ArgumentNullException>(value != null, nameof(value));
            Contract.Requires<ArgumentException>(Enum.IsDefined(value.GetType(), value), nameof(value));
            Contract.Ensures(Contract.Result<IEnumerable<TAttribute>>() != null);

            var type = value.GetType().GetTypeInfo();
            var fieldInfo = type.GetDeclaredField(value.ToString());
            return fieldInfo.GetCustomAttributes<TAttribute>();
        }
    }
}
