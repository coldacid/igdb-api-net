﻿namespace IgdbApi
{
    /// <summary>
    /// Represents meta information for the IGDB game franchises collection.
    /// </summary>
    public class FranchisesMetaInfo : MetaInfo
    {
    }
}
