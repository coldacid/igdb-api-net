﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Flurl;
using Flurl.Http;
using IgdbApi.Models;
using IgdbApi.Models.Wrappers;

namespace IgdbApi
{
    public partial class IgdbApi : IIgdbPlatformsApi
    {
        private static readonly string PlatformsUrl = BaseUrl.AppendPathSegment("platforms");

        /// <summary>
        /// Retrieves a list of platforms as an asynchronous operation.
        /// </summary>
        /// <param name="limit">The limit of how many results should be returned.</param>
        /// <param name="offset">The offset into the list of games where results should be returned from.</param>
        /// <returns>A list of platform summaries.</returns>
        /// <remarks>
        /// <para>If unset, <paramref name="limit"/> defaults to 25 results, which is the maximum allowed per call.</para>
        /// <para>If unset, <paramref name="offset"/> defaults to 0.</para>
        /// </remarks>
        public async Task<IEnumerable<PlatformSummary>> GetPlatformsAsync(int? limit, int? offset)
        {
            Url reqUrl = PlatformsUrl;
            if (limit.HasValue)
                reqUrl.SetQueryParam("limit", limit.Value);
            if (offset.HasValue)
                reqUrl.SetQueryParam("offset", offset.Value);

            using (var client = Client)
            {
                var list = await client.WithUrl(reqUrl).GetJsonAsync<GetListWrapper>();
                return list?.Platforms ?? Enumerable.Empty<PlatformSummary>();
            }
        }

        /// <summary>
        /// Retrieve's a specific platform's information as an asynchronous operation.
        /// </summary>
        /// <param name="platformId">The ID of the platform to look up.</param>
        /// <returns>The full information of the specified platform.</returns>
        public async Task<Platform> GetPlatformAsync(int platformId)
        {
            var reqUrl = PlatformsUrl.AppendPathSegment(platformId.ToString());

            using (var client = Client)
            {
                var wrapper = await client.WithUrl(reqUrl).GetJsonAsync<GetWrapper>();
                return wrapper?.Platform;
            }
        }

        /// <summary>
        /// Retrieves a list of games related to a specified company as an asynchronous operation.
        /// </summary>
        /// <param name="platformId">The ID of the platform whose games are being looked up.</param>
        /// <param name="limit">The limit of how many results should be returned.</param>
        /// <param name="offset">The offset into the list of games where results should be returned from.</param>
        /// <returns>A list of game summaries.</returns>
        /// <remarks>
        /// <para>If unset, <paramref name="limit"/> defaults to 25 results, which is the maximum allowed per call.</para>
        /// <para>If unset, <paramref name="offset"/> defaults to 0.</para>
        /// </remarks>
        public async Task<IEnumerable<GameSummary>> GetPlatformGamesAsync(int platformId, int? limit, int? offset)
        {
            var reqUrl = PlatformsUrl
                .AppendPathSegment(platformId.ToString())
                .AppendPathSegment("games");
            if (limit.HasValue)
                reqUrl.SetQueryParam("limit", limit.Value);
            if (offset.HasValue)
                reqUrl.SetQueryParam("offset", offset.Value);

            using (var client = Client)
            {
                var list = await client.WithUrl(reqUrl).GetJsonAsync<GetListWrapper>();
                return list?.Games ?? Enumerable.Empty<GameSummary>();
            }
        }

        /// <summary>
        /// Retrieves information about the list of companies known to IGDB.
        /// </summary>
        /// <returns>An object describing the meta information for the companies collection.</returns>
        public Task<PlatformsMetaInfo> GetPlatformsMetaInfoAsync()
        {
            using (var client = Client)
            {
                var reqUrl = PlatformsUrl.AppendPathSegment("meta");
                return client.WithUrl(reqUrl).GetJsonAsync<PlatformsMetaInfo>();
            }
        }
    }
}