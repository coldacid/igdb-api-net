﻿namespace IgdbApi
{
    /// <summary>
    /// Represents meta information for the IGDB people collection.
    /// </summary>
    public class PeopleMetaInfo : MetaInfo
    {
    }
}
