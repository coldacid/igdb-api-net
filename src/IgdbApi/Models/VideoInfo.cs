﻿using Newtonsoft.Json;

namespace IgdbApi.Models
{
    /// <summary>
    /// Represents a video stored on IGDB.
    /// </summary>
    public class VideoInfo
    {
        /// <summary>
        /// Gets the ID value of the video.
        /// </summary>
        [JsonProperty("uid")]
        public string Id { get; internal set; }

        /// <summary>
        /// Gets the title of the video.
        /// </summary>
        [JsonProperty("title")]
        public string Title { get; internal set; }
    }
}
