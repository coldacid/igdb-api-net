﻿using Newtonsoft.Json;

namespace IgdbApi.Models
{
    /// <summary>
    /// Represents a story theme exhibited in a game.
    /// </summary>
    public class ThemeInfo
    {
        /// <summary>
        /// Gets the name of the theme.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; internal set; }
    }
}
