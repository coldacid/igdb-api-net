﻿using Newtonsoft.Json;

namespace IgdbApi.Models.Wrappers
{
    internal class GetWrapper
    {
        [JsonProperty("game")]
        public Game Game { get; set; }

        [JsonProperty("company")]
        public Company Company { get; set; }

        [JsonProperty("franchise")]
        public Franchise Franchise { get; set; }

        [JsonProperty("platform")]
        public Platform Platform { get; set; }

        [JsonProperty("person")]
        public Person Person { get; set; }
    }
}
