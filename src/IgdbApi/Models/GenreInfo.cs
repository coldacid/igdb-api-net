﻿using Newtonsoft.Json;

namespace IgdbApi.Models
{
    /// <summary>
    /// Represents a genre of gameplay.
    /// </summary>
    public class GenreInfo
    {
        /// <summary>
        /// Gets the name of the genre.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; internal set; }
    }
}
