﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace IgdbApi.Models
{
    /// <summary>
    /// Represents a game.
    /// </summary>
    public class Game : GameBase
    {
        /// <summary>
        /// Gets a list of alternative names for the game.
        /// </summary>
        [JsonProperty("alternative_names")]
        public IList<AlternativeName> AlternativeNames { get; internal set; }

        /// <summary>
        /// Gets the date and time when the game was added to IGDB.
        /// </summary>
        [JsonProperty("created_at")]
        public DateTimeOffset? CreatedAt { get; internal set; }

        /// <summary>
        /// Gets the date and time when the game was last updated on IGDB.
        /// </summary>
        [JsonProperty("updated_at")]
        public DateTimeOffset? UpdatedAt { get; internal set; }

        /// <summary>
        /// Gets a summary describing the game.
        /// </summary>
        [JsonProperty("summary")]
        public string Summary { get; internal set; }

        /// <summary>
        /// Gets the list of genres for the game.
        /// </summary>
        [JsonProperty("genres")]
        public IList<GenreInfo> Genres { get; internal set; }

        /// <summary>
        /// Gets the list of themes of the game.
        /// </summary>
        [JsonProperty("themes")]
        public IList<ThemeInfo> Themes { get; internal set; }

        /// <summary>
        /// Gets the aggregate review rating of the game.
        /// </summary>
        [JsonProperty("rating")]
        public double Rating { get; internal set; }

        /// <summary>
        /// Gets the list of release dates and associated platforms for the game.
        /// </summary>
        [JsonProperty("release_dates")]
        public IList<ReleaseDateInfo> ReleaseDates { get; internal set; }

        /// <summary>
        /// Gets a list of companies involved in developing and publishing the game.
        /// </summary>
        [JsonProperty("companies")]
        public IList<GameCompanyInfo> Companies { get; internal set; }

        /// <summary>
        /// Gets details about the cover image for the game's page on IGDB.
        /// </summary>
        [JsonProperty("cover")]
        public ImageInfo Cover { get; internal set; }

        /// <summary>
        /// Gets a list of screenshots of the game on IGDB.
        /// </summary>
        [JsonProperty("screenshots")]
        public IList<ScreenshotInfo> Screenshots { get; internal set; }

        /// <summary>
        /// Gets a list of videos of the game on IGDB.
        /// </summary>
        [JsonProperty("videos")]
        public IList<VideoInfo> Videos { get; internal set; }
    }
}
