﻿using Newtonsoft.Json;

namespace IgdbApi.Models
{
    /// <summary>
    /// Represents the relationship between a game and the named company.
    /// </summary>
    public class GameCompanyInfo : CompanyBase
    {
        /// <summary>
        /// Gets whether or not the company was involved in the publishing of the game.
        /// </summary>
        [JsonProperty("publisher")]
        public bool IsPublisher { get; internal set; }

        /// <summary>
        /// Gets whether or not the company was involved in the development of the game.
        /// </summary>
        [JsonProperty("developer")]
        public bool IsDeveloper { get; internal set; }
    }
}
