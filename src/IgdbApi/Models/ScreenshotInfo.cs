﻿using Newtonsoft.Json;

namespace IgdbApi.Models
{
    /// <summary>
    /// Represents a screenshot of a game on IGDB.
    /// </summary>
    public class ScreenshotInfo : ImageInfo
    {
        /// <summary>
        /// Gets the title of the screenshot.
        /// </summary>
        [JsonProperty("title")]
        public string Title { get; internal set; }
    }
}
