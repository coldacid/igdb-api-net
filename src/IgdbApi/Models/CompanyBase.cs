﻿using Newtonsoft.Json;

namespace IgdbApi.Models
{
    /// <summary>
    /// Represents the basic properties of companies on IGDB.
    /// </summary>
    public class CompanyBase
    {
        /// <summary>
        /// Gets the ID value of the company.
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; internal set; }

        /// <summary>
        /// Gets the name of the company.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; internal set; }
    }
}
