﻿using Newtonsoft.Json;

namespace IgdbApi.Models
{
    /// <summary>
    /// Represents an alternative name for a game.
    /// </summary>
    public class AlternativeName
    {
        /// <summary>
        /// Gets the alternative name.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; internal set; }

        /// <summary>
        /// Gets a comment describing the name.
        /// </summary>
        [JsonProperty("comment")]
        public string Comment { get; internal set; }
    }
}
