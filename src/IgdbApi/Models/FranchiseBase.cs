﻿using Newtonsoft.Json;

namespace IgdbApi.Models
{
    /// <summary>
    /// Represents the basic properties of a game franchise on IGDB.
    /// </summary>
    public class FranchiseBase
    {
        /// <summary>
        /// Gets the ID value of the game franchise.
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; internal set; }

        /// <summary>
        /// Gets the URL slug for the game franchise's page on the IGDB website.
        /// </summary>
        [JsonProperty("slug")]
        public string Slug { get; internal set; }

        /// <summary>
        /// Gets the name of the game franchise.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; internal set; }
    }
}
