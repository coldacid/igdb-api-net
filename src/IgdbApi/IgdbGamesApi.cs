﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Flurl;
using Flurl.Http;
using IgdbApi.Models;
using IgdbApi.Models.Wrappers;
using IgdbApi.Search;

namespace IgdbApi
{
    public partial class IgdbApi : IIgdbGamesApi
    {
        private static readonly string GamesUrl = BaseUrl.AppendPathSegment("games");

        /// <summary>
        /// Retrieves a list of games as an asynchronous operation.
        /// </summary>
        /// <param name="limit">The limit of how many results should be returned.</param>
        /// <param name="offset">The offset into the list of games where results should be returned from.</param>
        /// <returns>A list of game summaries.</returns>
        /// <remarks>
        /// <para>If unset, <paramref name="limit"/> defaults to 25 results, which is the maximum allowed per call.</para>
        /// <para>If unset, <paramref name="offset"/> defaults to 0.</para>
        /// </remarks>
        public async Task<IEnumerable<GameSummary>> GetGamesAsync(int? limit, int? offset)
        {
            Url reqUrl = GamesUrl;
            if (limit.HasValue)
                reqUrl.SetQueryParam("limit", limit.Value);
            if (offset.HasValue)
                reqUrl.SetQueryParam("offset", offset.Value);

            using (var client = Client)
            {
                var list = await client.WithUrl(reqUrl).GetJsonAsync<GetListWrapper>();
                return list?.Games ?? Enumerable.Empty<GameSummary>();
            }
        }

        /// <summary>
        /// Retrieves a list of games matching the search query and optional search filters as an asynchronous operation.
        /// </summary>
        /// <param name="query">A query term to search the games with.</param>
        /// <param name="filters">A set of filters used to limit the result set.</param>
        /// <returns>A list of game summaries matching the search query and filters.</returns>
        public async Task<IEnumerable<GameSummary>> SearchGamesAsync(string query, params GameSearchFilter[] filters)
        {
            var reqUrl = GamesUrl
                .AppendPathSegment("search")
                .SetQueryParam("q", query);

            foreach (var filter in filters)
            {
                var fieldAttr = filter.Field.GetAttributes<SearchFilterQueryTermAttribute>().FirstOrDefault();
                var opAttr = filter.Operation.GetAttributes<SearchFilterQueryTermAttribute>().FirstOrDefault();

                var queryTerm = "filters[" + (fieldAttr?.Term ?? "name") + "_" + (opAttr?.Term ?? "eq") + "]";
                reqUrl.SetQueryParam(queryTerm, filter.Value);
            }

            using (var client = Client)
            {
                try
                {
                    var list = await client.WithUrl(reqUrl).GetJsonAsync<GetListWrapper>();
                    return list?.Games;
                }
                catch (FlurlHttpException ex)
                {
                    throw WrapFlurlHttpException(ex);
                }
            }
        }

        /// <summary>
        /// Retrieves a specific game's information as an asynchronous operation.
        /// </summary>
        /// <param name="gameId">The ID of the game to look up.</param>
        /// <returns>The full information of the specified game.</returns>
        public async Task<Game> GetGameAsync(int gameId)
        {
            var reqUrl = GamesUrl.AppendPathSegment(gameId.ToString());

            using (var client = Client)
            {
                var wrapper = await client.WithUrl(reqUrl).GetJsonAsync<GetWrapper>();
                return wrapper?.Game;
            }
        }

        /// <summary>
        /// Retrieves information about the list of games known to IGDB.
        /// </summary>
        /// <returns>An object describing the meta information for the games collection.</returns>
        public Task<GamesMetaInfo> GetGamesMetaInfoAsync()
        {
            using (var client = Client)
            {
                var reqUrl = GamesUrl.AppendPathSegment("meta");
                return client.WithUrl(reqUrl).GetJsonAsync<GamesMetaInfo>();
            }
        }
    }
}
