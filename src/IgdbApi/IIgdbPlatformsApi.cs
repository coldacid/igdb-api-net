﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Threading.Tasks;
using IgdbApi.Models;

namespace IgdbApi
{
    /// <summary>
    /// Defines the interface for querying platform information from the Internet Games Database.
    /// </summary>
    [ContractClass(typeof(IIgdbPlatformsApiContract))]
    public interface IIgdbPlatformsApi
    {
        /// <summary>
        /// Retrieves a list of platforms as an asynchronous operation.
        /// </summary>
        /// <param name="limit">The limit of how many results should be returned.</param>
        /// <param name="offset">The offset into the list of games where results should be returned from.</param>
        /// <returns>A list of platform summaries.</returns>
        /// <remarks>
        /// <para>If unset, <paramref name="limit"/> defaults to 25 results, which is the maximum allowed per call.</para>
        /// <para>If unset, <paramref name="offset"/> defaults to 0.</para>
        /// </remarks>
        Task<IEnumerable<PlatformSummary>> GetPlatformsAsync(int? limit = null, int? offset = null);

        /// <summary>
        /// Retrieve's a specific platform's information as an asynchronous operation.
        /// </summary>
        /// <param name="platformId">The ID of the platform to look up.</param>
        /// <returns>The full information of the specified platform.</returns>
        Task<Platform> GetPlatformAsync(int platformId);

        /// <summary>
        /// Retrieves a list of games related to a specified company as an asynchronous operation.
        /// </summary>
        /// <param name="platformId">The ID of the platform whose games are being looked up.</param>
        /// <param name="limit">The limit of how many results should be returned.</param>
        /// <param name="offset">The offset into the list of games where results should be returned from.</param>
        /// <returns>A list of game summaries.</returns>
        /// <remarks>
        /// <para>If unset, <paramref name="limit"/> defaults to 25 results, which is the maximum allowed per call.</para>
        /// <para>If unset, <paramref name="offset"/> defaults to 0.</para>
        /// </remarks>
        Task<IEnumerable<GameSummary>> GetPlatformGamesAsync(int platformId, int? limit = null, int? offset = null);

        /// <summary>
        /// Retrieves information about the list of companies known to IGDB.
        /// </summary>
        /// <returns>An object describing the meta information for the companies collection.</returns>
        Task<PlatformsMetaInfo> GetPlatformsMetaInfoAsync();
    }
}