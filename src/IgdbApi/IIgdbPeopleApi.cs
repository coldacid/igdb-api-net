﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Threading.Tasks;
using IgdbApi.Models;

namespace IgdbApi
{
    /// <summary>
    /// Defines the interface for querying people information from the Internet Games Database.
    /// </summary>
    [ContractClass(typeof(IIgdbPeopleApiContract))]
    public interface IIgdbPeopleApi
    {
        /// <summary>
        /// Retrieves a list of platforms as an asynchronous operation.
        /// </summary>
        /// <param name="limit">The limit of how many results should be returned.</param>
        /// <param name="offset">The offset into the list of games where results should be returned from.</param>
        /// <returns>A list of person summaries.</returns>
        /// <remarks>
        /// <para>If unset, <paramref name="limit"/> defaults to 25 results, which is the maximum allowed per call.</para>
        /// <para>If unset, <paramref name="offset"/> defaults to 0.</para>
        /// </remarks>
        Task<IEnumerable<PersonSummary>> GetPeopleAsync(int? limit = null, int? offset = null);

        /// <summary>
        /// Retrieve's a specific person's information as an asynchronous operation.
        /// </summary>
        /// <param name="personId">The ID of the person to look up.</param>
        /// <returns>The full information of the specified person.</returns>
        Task<Person> GetPersonAsync(int personId);

        /// <summary>
        /// Retrieves a list of games related to a specified person as an asynchronous operation.
        /// </summary>
        /// <param name="personId">The ID of the person whose games are being looked up.</param>
        /// <param name="limit">The limit of how many results should be returned.</param>
        /// <param name="offset">The offset into the list of games where results should be returned from.</param>
        /// <returns>A list of game summaries.</returns>
        /// <remarks>
        /// <para>If unset, <paramref name="limit"/> defaults to 25 results, which is the maximum allowed per call.</para>
        /// <para>If unset, <paramref name="offset"/> defaults to 0.</para>
        /// </remarks>
        Task<IEnumerable<GameSummary>> GetPersonGamesAsync(int personId, int? limit = null, int? offset = null);

        /// <summary>
        /// Retrieves information about the list of people known to IGDB.
        /// </summary>
        /// <returns>An object describing the meta information for the people collection.</returns>
        Task<PeopleMetaInfo> GetPeopleMetaInfoAsync();
    }
}