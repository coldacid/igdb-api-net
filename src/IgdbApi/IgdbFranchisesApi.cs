﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Flurl;
using Flurl.Http;
using IgdbApi.Models;
using IgdbApi.Models.Wrappers;

namespace IgdbApi
{
    public partial class IgdbApi : IIgdbFranchisesApi
    {
        private static readonly string FranchisesUrl = BaseUrl.AppendPathSegment("franchises");

        /// <summary>
        /// Retrieves a list of game franchises as an asynchronous operation.
        /// </summary>
        /// <param name="limit">The limit of how many results should be returned.</param>
        /// <param name="offset">The offset into the list of games where results should be returned from.</param>
        /// <returns>A list of game franchise summaries.</returns>
        /// <remarks>
        /// <para>If unset, <paramref name="limit"/> defaults to 25 results, which is the maximum allowed per call.</para>
        /// <para>If unset, <paramref name="offset"/> defaults to 0.</para>
        /// </remarks>
        public async Task<IEnumerable<FranchiseSummary>> GetFranchisesAsync(int? limit, int? offset)
        {
            Url reqUrl = FranchisesUrl;
            if (limit.HasValue)
                reqUrl.SetQueryParam("limit", limit.Value);
            if (offset.HasValue)
                reqUrl.SetQueryParam("offset", offset.Value);

            using (var client = Client)
            {
                var list = await client.WithUrl(reqUrl).GetJsonAsync<GetListWrapper>();
                return list?.Franchises ?? Enumerable.Empty<FranchiseSummary>();
            }
        }

        /// <summary>
        /// Retrieve's a specific game franchise's information as an asynchronous operation.
        /// </summary>
        /// <param name="franchiseId">The ID of the game franchise to look up.</param>
        /// <returns>The full information of the specified game franchise.</returns>
        public async Task<Franchise> GetFranchiseAsync(int franchiseId)
        {
            var reqUrl = FranchisesUrl.AppendPathSegment(franchiseId.ToString());

            using (var client = Client)
            {
                var wrapper = await client.WithUrl(reqUrl).GetJsonAsync<GetWrapper>();
                return wrapper?.Franchise;
            }
        }

        /// <summary>
        /// Retrieves a list of games related to a specified company as an asynchronous operation.
        /// </summary>
        /// <param name="franchiseId">The ID of the game franchise whose games are being looked up.</param>
        /// <param name="limit">The limit of how many results should be returned.</param>
        /// <param name="offset">The offset into the list of games where results should be returned from.</param>
        /// <returns>A list of game summaries.</returns>
        /// <remarks>
        /// <para>If unset, <paramref name="limit"/> defaults to 25 results, which is the maximum allowed per call.</para>
        /// <para>If unset, <paramref name="offset"/> defaults to 0.</para>
        /// </remarks>
        public async Task<IEnumerable<GameSummary>> GetFranchiseGamesAsync(int franchiseId, int? limit, int? offset)
        {
            var reqUrl = FranchisesUrl
                .AppendPathSegment(franchiseId.ToString())
                .AppendPathSegment("games");
            if (limit.HasValue)
                reqUrl.SetQueryParam("limit", limit.Value);
            if (offset.HasValue)
                reqUrl.SetQueryParam("offset", offset.Value);

            using (var client = Client)
            {
                var list = await client.WithUrl(reqUrl).GetJsonAsync<GetListWrapper>();
                return list?.Games ?? Enumerable.Empty<GameSummary>();
            }
        }

        /// <summary>
        /// Retrieves information about the list of companies known to IGDB.
        /// </summary>
        /// <returns>An object describing the meta information for the companies collection.</returns>
        public Task<FranchisesMetaInfo> GetFranchisesMetaInfoAsync()
        {
            using (var client = Client)
            {
                var reqUrl = FranchisesUrl.AppendPathSegment("meta");
                return client.WithUrl(reqUrl).GetJsonAsync<FranchisesMetaInfo>();
            }
        }
    }
}