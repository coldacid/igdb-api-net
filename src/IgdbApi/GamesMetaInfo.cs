﻿namespace IgdbApi
{
    /// <summary>
    /// Represents meta information for the IGDB games collection.
    /// </summary>
    public class GamesMetaInfo : MetaInfo {}
}
