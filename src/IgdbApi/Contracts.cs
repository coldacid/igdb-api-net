﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Threading.Tasks;
using IgdbApi.Models;
using IgdbApi.Search;

namespace IgdbApi
{
    [ContractClassFor(typeof(IIgdbGamesApi))]
    // ReSharper disable once InconsistentNaming
    internal abstract class IIgdbGamesApiContract : IIgdbGamesApi
    {
        public Task<Game> GetGameAsync(int gameId)
        {
            return null;
        }

        public Task<IEnumerable<GameSummary>> GetGamesAsync(int? limit, int? offset)
        {
            Contract.Ensures(Contract.Result<Task<IEnumerable<GameSummary>>>() != null);
            return null;
        }

        public Task<GamesMetaInfo> GetGamesMetaInfoAsync()
        {
            Contract.Ensures(Contract.Result<Task<GamesMetaInfo>>() != null);
            return null;
        }

        public Task<IEnumerable<GameSummary>> SearchGamesAsync(string query, params GameSearchFilter[] filters)
        {
            Contract.Requires<ArgumentNullException>(!String.IsNullOrWhiteSpace(query), nameof(query));
            Contract.Ensures(Contract.Result<Task<IEnumerable<GameSummary>>>() != null);
            return null;
        }
    }

    [ContractClassFor(typeof(IIgdbCompaniesApi))]
    // ReSharper disable once InconsistentNaming
    internal abstract class IIgdbCompaniesApiContract : IIgdbCompaniesApi
    {
        public Task<IEnumerable<CompanySummary>> GetCompaniesAsync(int? limit = null, int? offset = null)
        {
            Contract.Ensures(Contract.Result<Task<IEnumerable<CompanySummary>>>() != null);
            return null;
        }

        public Task<Company> GetCompanyAsync(int companyId)
        {
            return null;
        }

        public Task<IEnumerable<GameSummary>> GetCompanyGamesAsync(int companyId, int? limit = null, int? offset = null)
        {
            Contract.Ensures(Contract.Result<Task<IEnumerable<GameSummary>>>() != null);
            return null;
        }

        public Task<CompaniesMetaInfo> GetCompaniesMetaInfoAsync()
        {
            Contract.Ensures(Contract.Result<Task<CompaniesMetaInfo>>() != null);
            return null;
        }
    }

    [ContractClassFor(typeof(IIgdbFranchisesApi))]
    // ReSharper disable once InconsistentNaming
    internal abstract class IIgdbFranchisesApiContract : IIgdbFranchisesApi
    {
        public Task<IEnumerable<FranchiseSummary>> GetFranchisesAsync(int? limit = null, int? offset = null)
        {
            Contract.Ensures(Contract.Result<Task<IEnumerable<FranchiseSummary>>>() != null);
            return null;
        }

        public Task<Franchise> GetFranchiseAsync(int franchiseId)
        {
            return null;
        }

        public Task<IEnumerable<GameSummary>> GetFranchiseGamesAsync(int franchiseId, int? limit = null, int? offset = null)
        {
            Contract.Ensures(Contract.Result<Task<IEnumerable<GameSummary>>>() != null);
            return null;
        }

        public Task<FranchisesMetaInfo> GetFranchisesMetaInfoAsync()
        {
            Contract.Ensures(Contract.Result<Task<FranchisesMetaInfo>>() != null);
            return null;
        }
    }

    [ContractClassFor(typeof(IIgdbPlatformsApi))]
    // ReSharper disable once InconsistentNaming
    internal abstract class IIgdbPlatformsApiContract : IIgdbPlatformsApi
    {
        public Task<IEnumerable<PlatformSummary>> GetPlatformsAsync(int? limit = null, int? offset = null)
        {
            Contract.Ensures(Contract.Result<Task<IEnumerable<PlatformSummary>>>() != null);
            return null;
        }

        public Task<Platform> GetPlatformAsync(int platformId)
        {
            return null;
        }

        public Task<IEnumerable<GameSummary>> GetPlatformGamesAsync(int platformId, int? limit = null, int? offset = null)
        {
            Contract.Ensures(Contract.Result<Task<IEnumerable<GameSummary>>>() != null);
            return null;
        }

        public Task<PlatformsMetaInfo> GetPlatformsMetaInfoAsync()
        {
            Contract.Ensures(Contract.Result<Task<PlatformsMetaInfo>>() != null);
            return null;
        }
    }

    [ContractClassFor(typeof(IIgdbPeopleApi))]
    // ReSharper disable once InconsistentNaming
    internal abstract class IIgdbPeopleApiContract : IIgdbPeopleApi
    {
        public Task<IEnumerable<PersonSummary>> GetPeopleAsync(int? limit = null, int? offset = null)
        {
            Contract.Ensures(Contract.Result<Task<IEnumerable<PersonSummary>>>() != null);
            return null;
        }

        public Task<Person> GetPersonAsync(int personId)
        {
            return null;
        }

        public Task<IEnumerable<GameSummary>> GetPersonGamesAsync(int personId, int? limit = null, int? offset = null)
        {
            Contract.Ensures(Contract.Result<Task<IEnumerable<GameSummary>>>() != null);
            return null;
        }

        public Task<PeopleMetaInfo> GetPeopleMetaInfoAsync()
        {
            Contract.Ensures(Contract.Result<Task<PeopleMetaInfo>>() != null);
            return null;
        }
    }
}
