# IGDB

[![NuGet](https://buildstats.info/nuget/IgdbApi)](https://www.nuget.org/packages/IgdbApi/)
[![AppVeyor](https://ci.appveyor.com/api/projects/status/e6ckdonq99p60pye?svg=true)](https://ci.appveyor.com/project/coldacid/igdb-api-net)
[![Coveralls](https://coveralls.io/repos/bitbucket/coldacid/igdb-api-net/badge.svg?branch=develop)](https://coveralls.io/bitbucket/coldacid/igdb-api-net)

.NET wrapper for IGDB.com API. Requires an API key. Request one from your
settings screen.

Read more: https://www.igdb.com/api/v1/documentation
