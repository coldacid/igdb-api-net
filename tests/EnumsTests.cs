using System;
using System.Linq;
using Xunit;

namespace IgdbApi.Tests
{
    public class EnumsTests
    {
        public enum TestEnum
        {
            ItemA,
            ItemB,
            ItemC
        }

        [Flags]
        public enum TestFlags
        {
            FlagA = 1 << 0,
            FlagB = 1 << 1,
            FlagC = 1 << 2
        }

        public struct NotAnEnum { }

        public class GetValuesTests
        {
            [Fact]
            public void ThrowsIfTypeParamIsNotAnEnum()
            {
                var ex = Assert.Throws<InvalidOperationException>(() => Enums.GetValues<NotAnEnum>());
            }

            [Fact]
            public void ReturnsAllValuesInEnum()
            {
                var expected = new []
                {
                    TestEnum.ItemA,
                    TestEnum.ItemB,
                    TestEnum.ItemC
                };

                var values = Enums.GetValues<TestEnum>();

                Assert.Equal(expected, values);
            }
        }

        public class GetNameTests {
            [Fact]
            public void ThrowsIfTypeParamIsNotAnEnum()
            {
                var ex = Assert.Throws<InvalidOperationException>(() => (new NotAnEnum()).GetName());
            }

            [Theory]
            [InlineData(TestEnum.ItemA, "ItemA")]
            [InlineData(TestEnum.ItemB, "ItemB")]
            [InlineData(TestEnum.ItemC, "ItemC")]
            public void ReturnsNameOfEnumValue(TestEnum value, string expected)
            {
                Assert.Equal(expected, value.GetName());
            }
        }

        public class GetAttributesTests
        {
            public class TestAttribute : Attribute { }
            public enum TestAttributeEnum
            {
                [Test] ItemA,
                [Test] ItemB,
                /*  */ ItemC
            }

            [Fact]
            public void ThrowsIfValueIsNotDefined()
            {
                var ex = Assert.Throws<ArgumentException>(
                    () => (TestFlags.FlagA | TestFlags.FlagB).GetAttributes<Attribute>());
            }

            [Theory]
            [InlineData(TestAttributeEnum.ItemA)]
            [InlineData(TestAttributeEnum.ItemB)]
            public void ReturnsEnumerableOfRequestedAttributes(TestAttributeEnum value)
            {
                var attrs = value.GetAttributes<TestAttribute>();
                Assert.NotEmpty(attrs);
            }

            [Theory]
            [InlineData(TestAttributeEnum.ItemC)]
            public void ReturnsEmptyEnumerableIfValueDoesNotHaveRequestedAttributes(TestAttributeEnum value)
            {
                var attrs = value.GetAttributes<TestAttribute>();
                Assert.Empty(attrs);
            }
        }
    }
}