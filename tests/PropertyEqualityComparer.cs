﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace IgdbApi.Tests
{
    class PropertyEqualityComparer<T> : IEqualityComparer<T>, IEqualityComparer
    {
        private static readonly Type Type = typeof (T);

        public bool Equals(T x, T y)
        {
            if (x == null || y == null)
                return (x == null && y == null);

            if (x.GetType() != y.GetType())
                return false;

            var propertyList = Type.GetRuntimeProperties();
            foreach (var property in propertyList)
            {
                var propType = property.PropertyType;

                var xProp = property.GetValue(x);
                var yProp = property.GetValue(y);

                if (xProp == null && yProp == null)
                {
                    continue;
                }
                if (xProp == null || yProp == null)
                {
                    return false;
                }

                if (typeof(IComparable).IsAssignableFrom(propType))
                {
                    IComparable
                        xVal = (IComparable) xProp,
                        yVal = (IComparable) yProp;

                    if (xVal.CompareTo(yVal) != 0)
                        return false;
                }
                else
                {
                    var childComparerType = GetType().GetGenericTypeDefinition().MakeGenericType(propType);
                    var childComparer = childComparerType
                        .GetConstructor(BindingFlags.CreateInstance, null, CallingConventions.Any, new Type[0], new ParameterModifier[0])
                        .Invoke(new object[0]) as IEqualityComparer;
                    if (childComparer == null)
                        return xProp.Equals(yProp);

                    if (!childComparer.Equals(xProp, yProp))
                        return false;
                }
            }
            return true;
        }

        public int GetHashCode(T obj)
        {
            if (obj == null) return 0;
            return obj.GetHashCode();
        }

        public new bool Equals(object x, object y)
        {
            try
            {
                return Equals((T) x, (T) y);
            }
            catch (InvalidCastException)
            {
                return false;
            }
        }

        public int GetHashCode(object obj)
        {
            if (obj == null) return 0;
            return obj.GetHashCode();
        }
    }
}
