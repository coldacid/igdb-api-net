﻿using System.Linq;
using System.Threading.Tasks;
using IgdbApi.Models;
using Xunit;

namespace IgdbApi.Tests
{
    public class PeopleApiTests
    {
        public abstract class PeopleApiTestBase : TestBase
        {
            protected IIgdbPeopleApi PeopleApi { get; }

            protected PeopleApiTestBase()
            {
                PeopleApi = new IgdbApi("apiKey");
            }
        }

        public class GetPeopleAsyncTests : PeopleApiTestBase
        {
            [Fact]
            public async Task CallsCorrectEndpoint()
            {
                HttpTest.RespondWithJson(200, new {people = new PersonSummary[0]});

                await PeopleApi.GetPeopleAsync();

                var call = HttpTest.CallLog[0];
                Assert.Equal("https://www.igdb.com/api/v1/people", call.Url);
            }

            [Fact]
            public async Task CorrectlyProcessesResults()
            {
                var people = new[] {new PersonSummary { Id = 1}};
                HttpTest.RespondWithJson(200, new {people});

                var result = await PeopleApi.GetPeopleAsync();

                Assert.Equal(people, result, new PropertyEqualityComparer<PersonSummary>());
            }

            [Fact]
            public async Task LimitParamSetsLimitQueryParam()
            {
                HttpTest.RespondWithJson(200, new {people = new PersonSummary[0]});

                await PeopleApi.GetPeopleAsync(limit: 5);

                var call = HttpTest.CallLog[0];
                Assert.Equal("5", call.GetQueryParams()["limit"]);
            }

            [Fact]
            public async Task OffsetParamSetsOffsetQueryParam()
            {
                HttpTest.RespondWithJson(200, new {people = new PersonSummary[0]});

                await PeopleApi.GetPeopleAsync(offset: 50);

                var call = HttpTest.CallLog[0];
                Assert.Equal("50", call.GetQueryParams()["offset"]);
            }

            [Fact]
            public async Task BothLimitAndOffsetAppearInQueryParam()
            {
                HttpTest.RespondWithJson(200, new {people = new PersonSummary[0]});

                await PeopleApi.GetPeopleAsync(limit: 10, offset: 100);

                var call = HttpTest.CallLog[0];
                var queryParams = call.GetQueryParams();
                Assert.Equal("10", queryParams["limit"]);
                Assert.Equal("100", queryParams["offset"]);
            }
        }

        public class GetPersonAsyncTests : PeopleApiTestBase
        {
            [Fact]
            public async Task CallsCorrectEndpoint()
            {
                HttpTest.RespondWithJson(200, new GamesMetaInfo());

                await PeopleApi.GetPersonAsync(1);

                var call = HttpTest.CallLog[0];
                Assert.Equal("https://www.igdb.com/api/v1/people/1", call.Url);
            }

            [Fact]
            public async Task CorrectlyProcessesResults()
            {
                var person = new Person { Id = 1 };
                HttpTest.RespondWithJson(200, new {person});

                var result = await PeopleApi.GetPersonAsync(1);

                Assert.Equal(person, result, new PropertyEqualityComparer<Person>());
            }
        }

        public class GetPersonGamesAsyncTests : PeopleApiTestBase
        {
            [Fact]
            public async Task CallsCorrectEndpoint()
            {
                HttpTest.RespondWithJson(200, new GamesMetaInfo());

                await PeopleApi.GetPersonGamesAsync(1);

                var call = HttpTest.CallLog[0];
                Assert.Equal("https://www.igdb.com/api/v1/people/1/games", call.Url);
            }

            [Fact]
            public async Task CorrectlyProcessesResults()
            {
                var games = new[] {new GameSummary {Id = 1}};
                HttpTest.RespondWithJson(200, new {games});

                var result = await PeopleApi.GetPersonGamesAsync(1);

                Assert.Equal(games, result, new PropertyEqualityComparer<GameSummary>());
            }

            [Fact]
            public async Task LimitParamSetsLimitQueryParam()
            {
                HttpTest.RespondWithJson(200, new {games = new GameSummary[0]});

                await PeopleApi.GetPersonGamesAsync(1, limit: 5);

                var call = HttpTest.CallLog[0];
                Assert.Equal("5", call.GetQueryParams()["limit"]);
            }

            [Fact]
            public async Task OffsetParamSetsOffsetQueryParam()
            {
                HttpTest.RespondWithJson(200, new {games = new GameSummary[0]});

                await PeopleApi.GetPersonGamesAsync(1, offset: 50);

                var call = HttpTest.CallLog[0];
                Assert.Equal("50", call.GetQueryParams()["offset"]);
            }

            [Fact]
            public async Task BothLimitAndOffsetAppearInQueryParam()
            {
                HttpTest.RespondWithJson(200, new {games = new GameSummary[0]});

                await PeopleApi.GetPersonGamesAsync(1, limit: 10, offset: 100);

                var call = HttpTest.CallLog[0];
                var queryParams = call.GetQueryParams();
                Assert.Equal("10", queryParams["limit"]);
                Assert.Equal("100", queryParams["offset"]);
            }
        }

        public class GetPeopleMetaInfoAsyncTests : PeopleApiTestBase
        {
            [Fact]
            public async Task CallsCorrectEndpoint()
            {
                HttpTest.RespondWithJson(200, new PeopleMetaInfo());

                await PeopleApi.GetPeopleMetaInfoAsync();

                var call = HttpTest.CallLog[0];
                Assert.Equal("https://www.igdb.com/api/v1/people/meta", call.Url);
            }

            [Fact]
            public async Task CorrectlyProcessesResults()
            {
                var metaInfo = new PeopleMetaInfo {Size = 1};
                HttpTest.RespondWithJson(200, metaInfo);

                var result = await PeopleApi.GetPeopleMetaInfoAsync();

                Assert.Equal(metaInfo, result, new PropertyEqualityComparer<PeopleMetaInfo>());
            }
        }
    }
}
