﻿using Flurl;
using Flurl.Http;

namespace IgdbApi.Tests
{
    internal static class TestHelpers
    {
        public static QueryParamCollection GetQueryParams(this HttpCall call)
        {
            if (call == null)
                return new QueryParamCollection();

            var requestUri = call.Request.RequestUri;
            var url = new Url(requestUri.ToString());

            return url.QueryParams;
        }
    }
}
