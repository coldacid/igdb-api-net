﻿using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace IgdbApi.Tests
{
    public class IgdbApiTests : TestBase
    {
        private readonly IIgdbApi _igdbApi;

        public IgdbApiTests()
        {
            _igdbApi = new IgdbApi("apiKey");
        }

        [Fact]
        public void ProvidesApiKeyFromConstructorAsApiKeyProperty()
        {
            Assert.Equal("apiKey", _igdbApi.ApiKey);
        }

        [Fact]
        public async Task ApiReusesKeyOnEveryRequest()
        {
            HttpTest
                .RespondWithJson(200, new {games = new object[0]})
                .RespondWithJson(200, new {games = new object[0]});

            await Task.WhenAll
            (
                _igdbApi.GetGamesAsync(),
                _igdbApi.GetGamesAsync()
            );

            Assert.True(HttpTest.CallLog.All(c => c.Request.Headers.Authorization != null
                                                  && c.Request.Headers.Authorization.Scheme == "Token"
                                                  && c.Request.Headers.Authorization.Parameter == "token=\"apiKey\""));
        }
    }
}
